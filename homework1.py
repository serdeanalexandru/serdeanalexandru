__author__ = 'student'

import random
deck=[]

def create():
    #creates the deck
    types=['spades','clubs','hearts','diamonds']
    for i in range(2, 14):
        for t in types:
            card={'no':i, 'type':t}
            deck.append(card)
    return


def print_card():
    #prints a random card from the deck
    print "A random card from the deck is:", str(deck[random.randint(0,47)])


def compare_cards():
    #compares two random cards from the deck
    card1=deck[random.randint(0,47)]
    card2=deck[random.randint(0,47)]

    if (card1['no']==card2['no']):
        print card1, "is equal to ", card2
    elif (card1['no'] > card2['no']):
        print card1, "is bigger than", card2
    else:
        print card1, "is smaller than", card2

create()
print_card()
compare_cards()